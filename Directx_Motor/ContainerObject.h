#pragma once
#include "SpaceObject.h"
#include "Object.h"
#include <vector>
using namespace std;

class ContainerObject: public SpaceObject {
private:
	int NumObjects;
	vector <SpaceObject*> ListaObjetos;

public:
	ContainerObject();
	ContainerObject(wstring D);

	int AddObjeto(SpaceObject* Obj);
	void RemoverObjeto(SpaceObject* Obj);
	int BuscarHijo(SpaceObject* Obj);
	int RetornarTamaņo();

	void OnUpdate()override;
	void OnDraw()override;
	void OnKey(unsigned char Key, int x, int y)override;
	void OnMouse(int button, int state, int x, int y)override;

	/*void OnUpdate();
	void OnDraw();
	void OnKey(unsigned char Key, int x, int y);
	void OnMouse(int button, int state, int x, int y);*/
};

