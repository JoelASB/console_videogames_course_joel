#pragma once
#include <iostream>
class Vector;
using namespace std;


class Matriz {
public:
	int f;
	int c;
	float** MatrizM;
public:
	Matriz();
	void Nulo();
	void Identidad();
	void Imprimir();
	friend Matriz operator+(Matriz& M_Sum1, Matriz& M_Sum2);
	friend Matriz operator-(Matriz& M_Sum1, Matriz& M_Sum2);
	friend Matriz operator*(Matriz& M_Sum1, Matriz& M_Sum2);
	friend Matriz operator*(Matriz& M_Sum, Vector& v);
	friend Matriz operator*(Matriz& M_Sum, float& e);
};