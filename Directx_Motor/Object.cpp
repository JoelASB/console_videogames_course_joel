#include "pch.h"
#include "Object.h"

#include <ppltasks.h>
#include <synchapi.h>

Object::Object() {
	Direccion = L"Assets\\Perro.obj";

	this->mesh = std::make_unique<WaveFrontReader<uint16_t>>();
	this->mesh->Load(Direccion, false);

	Vertex = this->mesh->vertices.size();
	Index = this->mesh->indices.size();
	//Id = 1;
}

Object::Object(std::wstring D) {
	Direccion = D.c_str();

	this->mesh = std::make_unique<WaveFrontReader<uint16_t>>();
	this->mesh->Load(Direccion, false);

	Vertex = this->mesh->vertices.size();
	Index = this->mesh->indices.size();
	//Id = 1;
}
/*Object::Object(int Id_Object) {
	Id = Id_Object;
}*/

/*int Object::Return_Vertex() {
	return Vertex;
}

int Object::Return_Index() {
	return Index;
}*/