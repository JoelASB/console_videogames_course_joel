#include "pch.h"
#include "ContainerObject.h"

ContainerObject::ContainerObject() : SpaceObject() {
	NumObjects = 0;
	ListaObjetos = vector<SpaceObject*>();
}

ContainerObject::ContainerObject(wstring D) : SpaceObject(D) {
	NumObjects = 0;
	ListaObjetos = vector<SpaceObject*>();
}

int ContainerObject::AddObjeto(SpaceObject* Obj) {
	ListaObjetos.push_back(Obj);
	//Obj->Pariente = this;
	NumObjects++;

	return ListaObjetos.size();

}

void ContainerObject::RemoverObjeto(SpaceObject* Obj) {
	int Index = BuscarHijo(Obj);
	if (Index > -1) {
		ListaObjetos.push_back(Obj);
		NumObjects--;
	}

}
int ContainerObject::BuscarHijo(SpaceObject* Obj) {

	for (int i = 0; i < ListaObjetos.size(); i++) {
		if (ListaObjetos[i] == Obj) {
			return i;
		}
	}

	return -1;
}
int ContainerObject::RetornarTamaņo() {
	return NumObjects;
}

void ContainerObject::OnUpdate() {
	//SpaceObject::OnUpdate();
	for (int ActualiHijos = 0; ActualiHijos < ListaObjetos.size(); ActualiHijos++) {
		ListaObjetos[ActualiHijos]->OnUpdate();
	}
}

void ContainerObject::OnDraw() {
	//SpaceObject::OnDraw();
	for (int ActualiHijos = 0; ActualiHijos < ListaObjetos.size(); ActualiHijos++) {
		ListaObjetos[ActualiHijos]->OnDraw();
	}
}

void ContainerObject::OnKey(unsigned char Key, int x, int y) {
	//SpaceObject::OnKey(Key, x, y);
	for (int ActualiHijos = 0; ActualiHijos < ListaObjetos.size(); ActualiHijos++) {
		ListaObjetos[ActualiHijos]->OnKey(Key, x, y);
	}
}

void ContainerObject::OnMouse(int button, int state, int x, int y) {
	//SpaceObject::OnMouse(button, state, x, y);
	for (int ActualiHijos = 0; ActualiHijos < ListaObjetos.size(); ActualiHijos++) {
		ListaObjetos[ActualiHijos]->OnMouse(button, state, x, y);
	}
}
