#pragma once
#include <iostream>

class Matriz;
using namespace std;


class Vector {

public:
	float x;
	float y;
	float z;
	float w;

public:
	Vector();
	Vector(float px, float py, float pz, float pw);
	Vector(const Vector& v);
	void Rellenar();
private:
	void VectorVacio();
public:
	void print();
	friend Vector operator+(const Vector& v1, const Vector& v2);
	friend Vector operator-(const Vector& v1, const Vector& v2);
	friend Vector operator*(float Multi, Vector& v);
	friend Vector operator*(Vector& v, Matriz& M_Sum);
	float Magnitud();
	void Normalizacion();
	Vector Normalizado();

};

