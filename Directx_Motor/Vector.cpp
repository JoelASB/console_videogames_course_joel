#include "pch.h"
#include "Vector.h"
#include "Matriz.h"
Vector::Vector() {
	x = 1;
	y = 1;
	z = 1;
	w = 1;

}
Vector::Vector(float px, float py, float pz, float pw) {
	x = px;
	y = py;
	z = pz;
	w = pw;

}
Vector::Vector(const Vector& v) {
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;

}

void Vector::VectorVacio() {
	x = 0;
	y = 0;

}

void Vector::Rellenar() {
	cout << "(" << x << ", " << y << ")\n";
}
Vector operator+(const Vector& v1, const Vector& v2) {
	return Vector(v1.x + v2.x,
					v1.y + v2.y,
					v1.z + v2.z,
					v1.w + v2.w);
}
Vector operator-(const Vector& v1, const Vector& v2) {
	return Vector(v1.x - v2.x,
					v1.y - v2.y,
					v1.z - v2.z,
					v1.w - v2.w);
}
Vector operator*(float Multi, Vector& v) {
	return Vector(v.x * Multi, v.y * Multi, v.z * Multi, v.w * Multi);
}





Vector operator*(Vector& v, Matriz& M_Sum) {


	float CoordenX = (M_Sum.MatrizM[0][0] * v.x) + (M_Sum.MatrizM[0][1] * v.y) + (M_Sum.MatrizM[0][2] * v.z) + M_Sum.MatrizM[0][3];
	float CoordenY = (M_Sum.MatrizM[1][0] * v.x) + (M_Sum.MatrizM[1][1] * v.y) + (M_Sum.MatrizM[1][2] * v.z) + M_Sum.MatrizM[1][3];

	float CoordenZ = (M_Sum.MatrizM[2][0] * v.x) + (M_Sum.MatrizM[2][1] * v.y) + (M_Sum.MatrizM[2][2] * v.z) + M_Sum.MatrizM[2][3];
	float CoordenW = (M_Sum.MatrizM[3][0] * v.x) + (M_Sum.MatrizM[3][1] * v.y) + (M_Sum.MatrizM[3][2] * v.z) + M_Sum.MatrizM[3][3];

	return Vector(CoordenX, CoordenY,CoordenZ,CoordenW);
}





float Vector::Magnitud() {
	return sqrt((x * x) + (y * y) + (z * z));
}
void Vector::Normalizacion() {
	cout << "(" << x / Magnitud() << ", " << y / Magnitud() << ", " << z / Magnitud() << ")\n";
}
Vector Vector::Normalizado() {
	float Magni = Magnitud();
	Magni = (Magni != 0) ? Magni : 1;
	return Vector(this->x / Magni, this->y / Magni, this->z / Magni, this->w / Magni);
}
void Vector::print() {
	cout << "(" << x << ", " << y << ", " << z << ")\n";
}