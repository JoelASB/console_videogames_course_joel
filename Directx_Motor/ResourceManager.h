#pragma once
#include <iostream>
#include <map>
#include <cstdio>

#include "RenderObject.h"

using namespace std;

class ResourceManager
{
private:
	//map <string, RenderObject*> Renders;
	static ResourceManager* instance;
	ResourceManager();

public:

	// Puntero almacenado en cach� para los recursos del dispositivo.
	std::shared_ptr<DX::DeviceResources> m_deviceResources;

	static ResourceManager* GetInstance();      // instancia el manejador de recursos
	//static std::shared_ptr<DX::DeviceResources>& device_Resource();

	//void SaveRender(string Direc);             // guarda el render
	//RenderObject* LoadRender(string Direc);         // carga el render
};

