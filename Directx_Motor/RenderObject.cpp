#include "pch.h"
#include "RenderObject.h"

#include "..\Common\DirectXHelper.h"
#include <ppltasks.h>
#include <synchapi.h>

using namespace Directx_Motor;

using namespace Concurrency;
using namespace DirectX;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::Storage;

RenderObject::RenderObject() :ContainerObject() {
	m_deviceResources = ResourceManager::GetInstance()->m_deviceResources;
}
RenderObject::RenderObject(wstring D) : ContainerObject(D) {
	m_deviceResources = ResourceManager::GetInstance()->m_deviceResources;
}

Directx_Motor::VertexPositionColor* RenderObject::MeshVertex() {
	Directx_Motor::VertexPositionColor* meshVertex = new Directx_Motor::VertexPositionColor[Vertex];

	for (int v = 0; v < Vertex; v++) {
		meshVertex[v] = { DirectX::XMFLOAT3(mesh->vertices[v].position.x,
									mesh->vertices[v].position.y,
									mesh->vertices[v].position.z),
							DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f),
							DirectX::XMFLOAT3(mesh->vertices[v].normal.x,
									mesh->vertices[v].normal.y,
									mesh->vertices[v].normal.z) };
	}

	return meshVertex;
}
unsigned short* RenderObject::MeshIndex() {
	unsigned short* meshIndex = new unsigned short[Index];

	for (int i = 0; i < Index; i++) {
		meshIndex[i] = mesh->indices[i];
	}
	return meshIndex;
}

int RenderObject::Return_Vertex() {
	return Vertex;
}
int RenderObject::Return_Index() {
	return Index;
}

auto RenderObject::Cargar_Sombreadores(std::wstring D) {

	//m_deviceResources = ResourceManager::GetInstance()->m_deviceResources;

	auto createVSTask = DX::ReadDataAsync(D.c_str()).then([this](std::vector<byte>& fileData) {
		m_vertexShader = fileData;
	});

	auto createPSTask = DX::ReadDataAsync(D.c_str()).then([this](std::vector<byte>& fileData) {
		m_pixelShader = fileData;
	});

	
	
	// Crear el estado de la canalización una vez cargados los sombreadores.    ==> Lo Necesito
	auto createPipelineStateTask = (createPSTask && createVSTask).then([this]() {

		static const D3D12_INPUT_ELEMENT_DESC inputLayout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		};

		D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
		state.InputLayout = { inputLayout, _countof(inputLayout) };
		state.pRootSignature = m_rootSignature.Get();
		state.VS = CD3DX12_SHADER_BYTECODE(&m_vertexShader[0], m_vertexShader.size());
		state.PS = CD3DX12_SHADER_BYTECODE(&m_pixelShader[0], m_pixelShader.size());
		state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		state.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		state.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		state.SampleMask = UINT_MAX;
		state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		state.NumRenderTargets = 1;
		state.RTVFormats[0] = m_deviceResources->GetBackBufferFormat();
		state.DSVFormat = m_deviceResources->GetDepthBufferFormat();
		state.SampleDesc.Count = 1;

		DX::ThrowIfFailed(m_deviceResources->GetD3DDevice()->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&m_pipelineState)));

		// Los datos de los sombreadores se pueden eliminar una vez creado el estado de la canalización.
		m_vertexShader.clear();
		m_pixelShader.clear();
	});

	return createPipelineStateTask;

}

/*RenderObject* RenderObject::loadObject(const char* filename)
{
	RenderObject* Objeto = new RenderObject();
	//int width, height;
	//textura->bits = stbi_load(filename, &width, &height, nullptr, 4);
	//for (int i = 0; i < (height / 2); ++i) {
	//	for (int j = 0; j < (width * 4); ++j) {
	//		byte aux = textura->bits[i * (width * 4) + j];
	//		textura->bits[i * (width * 4) + j] = textura->bits[(height - (i + 1)) * (width * 4) + j];
	//		textura->bits[(height - (i + 1)) * (width * 4) + j] = aux;
	//	}
	//}
	//textura->width = width;
	//textura->height = height;

	//return success
	return Objeto;
}*/

