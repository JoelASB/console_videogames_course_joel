#pragma once
//#include "Object.h"
#include "ContainerObject.h"
#include "ResourceManager.h"

using namespace std;

class RenderObject: ContainerObject {

public:

	std::shared_ptr<DX::DeviceResources> m_deviceResources;
	//ResourceManager resource;
	//unsigned short* meshIndex;

public:
	RenderObject();
	RenderObject(std::wstring D);

	int Return_Vertex();
	int Return_Index();

	Directx_Motor::VertexPositionColor* MeshVertex();
	unsigned short* MeshIndex();

	auto Cargar_Sombreadores(std::wstring D);

	//static RenderObject* loadObject(const char* filename);

	
};

