// B�fer de constantes que almacena las tres matrices principales de columna b�sicas para componer la geometr�a.
cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix model;
	matrix view;
	matrix projection;
};

// Datos de v�rtice usados como entrada en el sombreador de v�rtices.
struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
};

// Datos de color por p�xel que han pasado por el sombreador de p�xeles.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
	float3 view: TEXCOORD0;
	float3 vertex: TEXCOORD1;
};

// Sombreador sencillo para el procesamiento de v�rtices en la GPU.
PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);


	// Transformar la posici�n del v�rtice en espacio proyectado.
	pos = mul(pos, model);
	output.vertex = pos;
	pos = mul(pos, view);
	pos = mul(pos, projection);
	output.pos = pos;

	float4 norm = float4(input.normal, 1.0f);
	norm = mul(norm, model);

	// Pasar el color sin modificaci�n.
	//output.color = input.color;
	//output.color = input.color * frac(sin(dot(input.pos,float2(1234.567,434767)))*79856.2654);
	output.normal = normalize(norm);
	output.color = input.color;
	output.view = view[3].xyz;

	return output;
}
