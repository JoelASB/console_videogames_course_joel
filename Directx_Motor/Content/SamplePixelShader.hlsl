// Datos de color por p�xel que han pasado por el sombreador de p�xeles.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
	float3 view: TEXCOORD0;
	float3 vertex: TEXCOORD1;
};

float ligths(PixelShaderInput input){
	float3 L_Directional = normalize(float3(1.0,-1.0,-1.0));

	float Intensidad = 0.0f;
	float I_Ambiental = 0.1f;

	float Const_Difussa = 0.7f;
	

	float I_difussa = Const_Difussa * max(dot(-L_Directional, input.normal), 0);
	
	float Const_Especular = 0.7f;
	float3 V = normalize(input.vertex - input.view);
	//float3 H = (-L_Directional) + (V);
	float3 R = reflect(L_Directional, normalize(input.normal));
	float n = 50;

	float I_Especular = Const_Especular * pow(max(dot(R, V), 0.0), n);

	Intensidad = I_Ambiental + I_difussa + I_Especular;
	//Intensidad = I_Ambiental + I_difussa;

	return Intensidad;

}

// Funci�n de paso por los datos de color (interpolado).
float4 main(PixelShaderInput input) : SV_TARGET
{
	float3 col = ligths(input);
	//return float4(input.color, 1.0f);
	return float4(col, 1.0f);
}
