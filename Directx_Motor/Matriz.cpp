#include "pch.h"
#include "Matriz.h"
#include "Vector.h"


Matriz::Matriz() {
	f = 4;
	c = 4;
	MatrizM = new float* [f];
	for (int fl = 0; fl < f; fl++) {
		MatrizM[fl] = new float[c];
	}
}

void Matriz::Nulo() {
	for (int x = 0; x < f; x++) {
		for (int y = 0; y < c; y++) {
			MatrizM[x][y] = 0;
			cout << MatrizM[x][y];
		}
		cout << endl;
	}
}

void Matriz::Identidad() {
	for (int x = 0; x < f; x++) {
		for (int y = 0; y < c; y++) {
			if (x == y) {
				MatrizM[x][y] = 1;
			}
			else {
				MatrizM[x][y] = 0;
			}
		}
	}
}

void Matriz::Imprimir() {
	for (int x = 0; x < f; x++) {
		for (int y = 0; y < c; y++) {
			cout << MatrizM[x][y] << " ";
		}
		cout << endl;
	}
}

Matriz operator+(Matriz& M_Sum1, Matriz& M_Sum2) {
	Matriz Matrz = Matriz();
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			Matrz.MatrizM[i][j] = M_Sum1.MatrizM[i][j] + M_Sum2.MatrizM[i][j];
		}
	}
	return Matrz;
}

Matriz operator-(Matriz& M_Sum1, Matriz& M_Sum2) {
	Matriz Matrz = Matriz();
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			Matrz.MatrizM[i][j] = M_Sum1.MatrizM[i][j] - M_Sum2.MatrizM[i][j];
		}
	}
	return Matrz;
}

Matriz operator*(Matriz& M_Sum1, Matriz& M_Sum2) {
	Matriz Matrz = Matriz();
	for (int i = 0; i < Matrz.f; i++) {
		for (int j = 0; j < Matrz.c; j++) {
			Matrz.MatrizM[i][j] = M_Sum1.MatrizM[i][0] * M_Sum2.MatrizM[0][j] + M_Sum1.MatrizM[i][1] * M_Sum2.MatrizM[1][j]
									+ M_Sum1.MatrizM[i][2] * M_Sum2.MatrizM[2][j] + M_Sum1.MatrizM[i][3] * M_Sum2.MatrizM[3][j];
		}
	}
	return Matrz;
}

Matriz operator*(Matriz& M_Sum, Vector& v) {
	Matriz Matrz = Matriz();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			int Vec = 0;
			if (j == 0) {
				Vec = v.x;
			}
			if (j == 1) {
				Vec = v.y;
			}
			if (j == 2) {
				Vec = v.z;
			}
			Matrz.MatrizM[i][j] = M_Sum.MatrizM[i][j] * Vec;
		}
	}
	return Matrz;
}

Matriz operator*(Matriz& M_Sum, float& e) {
	Matriz Matrz = Matriz();
	for (int i = 0; i < Matrz.f; i++) {
		for (int j = 0; j < Matrz.c; j++) {
			Matrz.MatrizM[i][j] = M_Sum.MatrizM[i][j] * e;
		}
	}

	return Matrz;
}