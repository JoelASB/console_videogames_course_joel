#include "pch.h"
#include "SpaceObject.h"
SpaceObject::SpaceObject() {
	Pariente = NULL;
	Posicion.x = 0;
	Posicion.y = 0;

	Escala.x = 1;
	Escala.y = 1;

	Rotacion = 0.0f;
	Transformacion = new Transform(this);
	width = height = 1;
}

SpaceObject::SpaceObject(wstring D) : Object(D) {
	Pariente = NULL;
	Posicion.x = 0;
	Posicion.y = 0;

	Escala.x = 1;
	Escala.y = 1;

	Rotacion = 0.0f;
	Transformacion = new Transform(this);
	width = height = 1;
}

/*SpaceObject::SpaceObject(SpaceObject* Parient, wstring D): Object(D) {
	Pariente = Parient;
	Posicion.x = 0;
	Posicion.y = 0;

	Escala.x = 1;
	Escala.y = 1;

	Rotacion = 0.0f;
	Transformacion = new Transform(this);
	width = height = 1;
}*/

void SpaceObject::OnUpdate() {
	Transformacion->UpdateTransform();
}
void SpaceObject::OnDraw() {

}
void SpaceObject::OnKey(unsigned char Key, int x, int y) {

}
void SpaceObject::OnMouse(int button, int state, int x, int y) {

}
/*SpaceObject::~SpaceObject() {

}*/