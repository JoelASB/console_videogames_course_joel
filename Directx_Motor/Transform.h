#pragma once
#include "Matriz.h"
#include "Vector.h"
#include "SpaceObject.h"
#define PI 3.14156786

class SpaceObject;


class Transform
{
public:
	Matriz Posicion;
	Matriz Rotacion;
	Matriz Escala;

	Matriz Acumulador;
	Matriz transform;

	SpaceObject* SpObject;

	void SetMatriz();

	void UpdatePosicion();

	void UpdateRotacion();

	void UpdateEscala();

	void UpdateTransform();


public:
	Transform();
	Transform(SpaceObject* Trnsfrm);


	Vector LocalToGlobal(Vector Punto);
	Vector LocalToGlobal(float CoordenX, float CoordenY, float CoordenZ, float CoordenW);

};

