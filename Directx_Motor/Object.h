#pragma once
#include "WaveFrontReader.h"

#include "..\Common\DeviceResources.h"
#include "Content\ShaderStructures.h"
#include "..\Common\StepTimer.h"
//#include <ppltasks.h>



class Object
{
/*private:
	unsigned Id;*/


protected:
	std::unique_ptr<WaveFrontReader<uint16_t>> mesh;
	const wchar_t* Direccion;

	int Vertex;
	int Index;

	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	m_commandList;
	Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_rootSignature;
	Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_pipelineState;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_cbvHeap;
	Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBuffer;
	Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer;

	Directx_Motor::ModelViewProjectionConstantBuffer	m_constantBufferData;

	UINT8* m_mappedConstantBuffer;
	UINT												m_cbvDescriptorSize;
	D3D12_RECT											m_scissorRect;
	std::vector<byte>									m_vertexShader;
	std::vector<byte>									m_pixelShader;
	D3D12_VERTEX_BUFFER_VIEW							m_vertexBufferView;
	D3D12_INDEX_BUFFER_VIEW								m_indexBufferView;


	//Microsoft::WRL::ComPtr<ID3D12RootSignature>	m_rootSignature;

	/*std::shared_ptr<DX::DeviceResources> m_deviceResources;
	std::vector<byte> m_vertexShader;
	std::vector<byte> m_pixelShader;*/

public:
	Object();
	Object(std::wstring D);

	//int Return_Vertex();
	//int Return_Index();

	/*Object(int Id);
	unsigned GetID();*/
};

