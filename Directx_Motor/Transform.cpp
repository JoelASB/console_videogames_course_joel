#include "pch.h"
#include "Transform.h"
Transform::Transform() {
	SpObject = NULL;
	Posicion = Matriz();
	Rotacion = Matriz();
	Escala = Matriz();

	Acumulador = Matriz();

	transform = Matriz();

}
Transform::Transform(SpaceObject* Trnsfrm) {
	this->SpObject = Trnsfrm;
}

void Transform::SetMatriz() {
	Posicion.Identidad();
	Posicion.MatrizM[0][2] = SpObject->Posicion.x;
	Posicion.MatrizM[1][2] = SpObject->Posicion.y;
	Escala.Identidad();
	Escala.MatrizM[0][0] = SpObject->Escala.x;
	Escala.MatrizM[1][1] = SpObject->Escala.y;


	Rotacion.Identidad();
	float radianes = (this->SpObject->Rotacion / 180.0) * PI;
	Rotacion.MatrizM[0][0] = cos(radianes);
	Rotacion.MatrizM[0][1] = -sin(radianes);
	Rotacion.MatrizM[1][0] = sin(radianes);
	Posicion.MatrizM[1][1] = cos(radianes);

	Matriz M1 = Posicion * Escala;
	Matriz M2 = M1 * Rotacion;
	transform = M2;
}

void Transform::UpdatePosicion() {
	Posicion.Identidad();
	Posicion.MatrizM[0][2] = SpObject->Posicion.x;
	Posicion.MatrizM[1][2] = SpObject->Posicion.y;
}

void Transform::UpdateRotacion() {
	Rotacion.Identidad();
	float radianes = (this->SpObject->Rotacion / 180.0) * PI;

	Rotacion.MatrizM[0][0] = cos(radianes);
	Rotacion.MatrizM[0][1] = -sin(radianes);
	Rotacion.MatrizM[1][0] = sin(radianes);
	Rotacion.MatrizM[1][1] = cos(radianes);
}

void Transform::UpdateEscala() {
	Escala.Identidad();
	Escala.MatrizM[0][0] = SpObject->Escala.x;
	Escala.MatrizM[1][1] = SpObject->Escala.y;
}

void Transform::UpdateTransform() {
	transform.Identidad();

	UpdatePosicion();
	UpdateRotacion();
	UpdateEscala();

	Matriz M1 = Rotacion * Escala;
	Matriz M2 = Posicion * M1;
	transform = M2;


	Acumulador.Identidad();

	if (SpObject->Pariente == NULL) {
		Acumulador = transform;
	}
	else {
		Acumulador = SpObject->Pariente->Transformacion->Acumulador * transform;
	}




}
Vector Transform::LocalToGlobal(Vector Punto) {
	return Punto * Acumulador;
}
Vector Transform::LocalToGlobal(float CoordenX, float CoordenY, float CoordenZ, float CoordenW) {

	Vector V = Vector(CoordenX, CoordenY, CoordenZ, CoordenW);
	return V * Acumulador;
}


//Vector2D* Transform::LocalOGlobal(Vector2D* Punto) {
//	return Punto * Acumulador;
//}
//Vector2D* Transform::LocalOGlobal(float CoordenX, float CoordenY) {
//
//	Vector2D* V = new Vector2D(CoordenX, CoordenY, 1);
//	Vector2D* Retornar = V * Acumulador;
//	return V * Acumulador;
//}


