#pragma once
#include <iostream>
#include "Object.h"
#include "Vector.h"
#include "Transform.h"

class Transform;

class SpaceObject : public Object {

public:
	Vector Posicion;
	Vector Escala;
	float Rotacion, width, height;
	SpaceObject* Pariente;
	Transform* Transformacion;

	SpaceObject();
	//~SpaceObject();

public:
	SpaceObject(wstring D);
	//SpaceObject(SpaceObject* Parient, wstring D);
	virtual void OnUpdate();
	virtual void OnDraw();
	virtual void OnKey(unsigned char Key, int x, int y);
	virtual void OnMouse(int button, int state, int x, int y);

};

